# binclock Makefile by Nico Golde <binclock@ngolde.de>
#
# Copyright (C) 2004 Nico Golde <binclock@ngolde.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see the file COPYING); if not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA  02111-1307, USA
#

CC = gcc
CFLAGS = -O2 -Wall --pedantic
BIN = binclock
SRC = ./src/binclock.c
INSPATH = /usr/local/bin/
SOURCEPATH = ./
MANPATH = /usr/man/man1/
CONF = /etc/

all : $(MAINSRC)
	$(CC) $(CFLAGS) -o $(BIN) $(SRC)
	strip $(SOURCEPATH)$(BIN)

install :

	chmod 644 $(SOURCEPATH)doc/binclock.1
	cp -pf $(SOURCEPATH)doc/binclock.1 $(MANPATH)
	cp -pf $(BIN)rc $(HOME)/.$(BIN)rc
	cp -pf $(SOURCEPATH)$(BIN) $(INSPATH)
	cp -pf $(BIN)rc $(CONF)binclockrc
	install -c -s -m 0755 $(BIN) $(INSPATH)
		
clean : 
	rm -f $(SOURCEPATH)$(BIN)

uninstall : 
	rm -f $(INSPATH)binclock
	rm -f $(CONF)$(BIN)rc
	rm -f $(MANPATH)/binclock.1
	rm -f $(HOME)/.binclockrc
