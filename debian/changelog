binclock (1.5-11) unstable; urgency=medium

  * QA upload.
  * debian/control: added a VCS (Git) to control the package.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.

 -- Filipi Souza <filipi.saci@gmail.com>  Sat, 06 Jun 2020 09:04:28 -0300

binclock (1.5-10) unstable; urgency=medium

  * QA upload.
  * debian/copyright:
      - Migrated to 1.0 format.
      - Full update.
  * debian/patches:
      - 20-hardening.patch: created to fix hardening flags.
      - 30-build-cross.patch: creted to fix FTCBFS. Thanks to Helmut Grohne
        <helmut@subdivi.de> (Closes: #960290)
  * debian/rules: added all hardening flags.
  * debian/tests/control: Added 'Restrictions: superficial' for CI tests.

 -- Filipi Souza <filipi.saci@gmail.com>  Wed, 03 Jun 2020 22:13:29 -0300

binclock (1.5-9) unstable; urgency=medium

  * QA upload.
  * debian/control: added 'Rules-Requires-Root: no' to source stanza.
  * debian/patches/{*.patch}: added headers.
  * debian/upstream/metadata: created.
  * debian/watch: created.

 -- Filipi Souza <filipi.saci@gmail.com>  Tue, 19 May 2020 19:40:28 -0300

binclock (1.5-8) unstable; urgency=medium

  * QA upload.
  * debian/tests/control: created to perform six CI tests.

 -- Filipi Souza <filipi.saci@gmail.com>  Fri, 15 May 2020 18:24:10 -0300

binclock (1.5-7) unstable; urgency=medium

  * QA upload.
  * Dropped CDBS system.
  * debian/docs: created to install README.
  * Set Debian QA Group as maintainer. (see #842557)
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control: bumped Standards-Version to 4.5.0.

 -- Filipi Souza <filipi.saci@gmail.com>  Sat, 09 May 2020 15:07:12 -0300

binclock (1.5-6) unstable; urgency=low

  * Remove deprecated dpatch and upgrade to packaging format "3.0 quilt".
  * Update to Standards-Version to 3.9.3.
  * Fix copyright-refers-to-symlink-license (Lintian).
  * Fix manpage-has-bad-whatis-entry (Lintian) with patch 10.
  * Thanks a lot Jaari Aalot!
  (Closes: #669577)

 -- Nico Golde <nion@debian.org>  Fri, 20 Apr 2012 07:33:43 +0300

binclock (1.5-5) unstable; urgency=low

  * Bump compat level and debhelper dependency to 7.
  * Make Homepage a real control field.
  * Bump to policy version 3.8.0, no changes needed.
  * Add description to makefile.dpatch

 -- Nico Golde <nion@debian.org>  Tue, 22 Jul 2008 19:06:50 +0200

binclock (1.5-4) unstable; urgency=low

  * Commented out strip in Makefile (included in makefile.dpatch)
    to enable correct functionality for
    DEB_BUILD_OPTIONS=nostrip (Closes: #436587).

 -- Nico Golde <nion@debian.org>  Wed, 08 Aug 2007 16:02:02 +0200

binclock (1.5-3) unstable; urgency=low

  * Changed maintainer address.
  * Bumped standards version, no changes needed.
  * Added Homepage tag to control file.
  * Bumped compat to 5 and changed dependency
  * Removed menu entry, not needed for this text mode
    application here.
  * Fixed copyright file.

 -- Nico Golde <nion@debian.org>  Fri, 16 Mar 2007 12:15:17 +0100

binclock (1.5-2) unstable; urgency=low

  * Fixup upstream Makefile installing files in $HOME on buildtime

 -- Nico Golde <nico@ngolde.de>  Thu, 29 Jul 2004 19:07:26 +0200

binclock (1.5-1) unstable; urgency=low

  * New upstream Release
  * Package building now uses cdbs

 -- Nico Golde <nico@ngolde.de>  Sat, 24 Jul 2004 13:47:04 +0200

binclock (1.4-3) unstable; urgency=low

  * Rebuild with no changes
  * Bug fixed in the changelog file :)
  * Initial Release

 -- Nico Golde <binclock-maint@lists.berlios.de>  Tue, 20 Apr 2004 00:38:10 +0200
